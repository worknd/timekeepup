# Keep Focus

Time keep application.

***This application is not meant for production purposes. Please be careful with configuring the secrets for the database and SSL certificates.***

# Pre-requisites

  1) Docker with docker-compose
  2) SSL certificates

## Certificates

Create and add certificates for the application and the adminer service to the `secrets` folder.
Certificates naming and domains:
app.local: `app_local.crt` and `app_local.key`
adminer.local: `adminer_local.crt` and `adminer_local.key`

# Running

`docker-compose up` to see the progress of building

`docker-compose up -d` to run in background

Application page [app.local](https://app.local)

DB Admin page [adminer.local](https://adminer.local)

**To rebuild the application, run the `clean.sh` script**

# Config

PostgreSQL - `config/config.json`
Web Server Docker - `Dockerfile`
Orchestration - `docker-compose.yml`
Proxy Server - `nginx/nginx.conf`
