FROM node:current
RUN apt-get update && apt-get install -y netcat
WORKDIR /home/node/app/
RUN chown -R node:node /home
USER node
COPY --chown=node:node ./docker-entrypoint.sh /docker-entrypoint.sh
COPY --chown=node:node ./wait-for.sh /wait-for.sh
RUN ["chmod", "+x", "/docker-entrypoint.sh"]
RUN ["chmod", "+x", "/wait-for.sh"]
COPY --chown=node:node ./package.json package.json
COPY --chown=node:node ./secrets/app_local.crt /app_local.crt
COPY --chown=node:node ./secrets/app_local.key /app_local.key
COPY --chown=node:node . .
CMD [ "sh","-c", "/wait-for.sh db:5432 -- /docker-entrypoint.sh" ]
