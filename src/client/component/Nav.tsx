import React from 'react';
import { NavLink } from 'react-router-dom';
import Home from '../page/Home';
import RegisterUser from '../page/user/RegisterUser';
import { Menu } from 'semantic-ui-react';

export default function Nav (props) {
  console.log(props);
  return <Menu align="right">
    <Menu.Item as={NavLink} exact to="/">Home</Menu.Item>
    <Menu.Item as={NavLink} to="/register">Register</Menu.Item>
  </Menu>
}
