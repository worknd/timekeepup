import React, { Component, ReactNode } from 'react';
import { Form, Checkbox, Button, Input, Label, Dimmer, Loader, Segment, Container } from 'semantic-ui-react';
import User from '../../model/user'
import UserService from '../../service/user'

export default class RegisterUser extends Component {
  public state = {
    firstname: null,
    lastname: null,
    email: null,
    password: null,
    passwordConfirm: null,
    terms: false,
    firstnameError: false,
    lastnameError: false,
    emailError: false,
    passwordError: false,
    passwordConfirmError: false,
    termsError: false,
    loading: false
  };

  constructor(props: any) {
    super(props);
  }

  // componentDidMount() {
  // this.setState({
  //   counter: ++this.state.counter
  // })
  // }

  firstnameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const firstname = event.target.value
    let error = false
    if (!firstname || !firstname.length) {
      error = true
    }

    this.setState({
      firstname: firstname,
      firstnameError: error
    })
  }

  lastnameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const lastname = event.target.value
    let error = false
    if (!lastname || !lastname.length) {
      error = true
    }

    this.setState({
      lastname: lastname,
      lastnameError: error
    })
  }

  emailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const email = event.target.value
    const re = /\S+@\S+\.\S+/
    const valid = re.test(email)
    let error = false
    if (!email || !email.length || !valid) {
      error = true
    }

    this.setState({
      email: email,
      emailError: error
    })
  }

  passwordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const password = event.target.value
    const passConfirm = this.state.passwordConfirm
    let error = false;
    const confirmed = passConfirm === password
    if (!password || !password.length || !confirmed) {
      error = true
    }

    this.setState({
      password: password,
      passwordError: error,
      passwordConfirmError: !confirmed
    })
  }

  passConfirmChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const passConfirm = event.target.value
    const password = this.state.password
    let error = false;
    const confirmed = passConfirm === password
    if (!password || !password.length || !confirmed) {
      error = true
    }

    this.setState({
      passwordConfirm: password,
      passwordError: !confirmed,
      passwordConfirmError: error
    })
  }

  termsChange = (event: any, data: any) => {
    this.setState({
      terms: data.checked,
      termsError: !data.checked
    })
  }

  submit = (event: any, data: any) => {
    if (!this.state.terms) {
      this.setState({
        termsError: true
      });
      return false;
    }

    if (
      this.state.firstnameError ||
      this.state.lastnameError ||
      this.state.emailError ||
      this.state.passwordError ||
      this.state.passwordConfirmError
    ) {
      return false;
    }

    const userService = new UserService()
    const user = new User()
    user.setFirstname(this.state.firstname)
      .setLastname(this.state.lastname)
      .setEmail(this.state.email)
      .setPassword(this.state.password)

    this.setState({
      loading: true
    })
    userService.register(user)
      .then()
      .catch()
      .finally(() => {
        this.setState({
          loading: false
        })
      })
  }

  render(): ReactNode {
    return (
      <Container fluid >
        <Dimmer active={this.state.loading} page >
          <Loader />
        </Dimmer>
        <Form>
          <Form.Field>
            <label>First Name</label>
            <Input placeholder="First Name" name="firstName" minLength="1" maxLength="250" onChange={this.firstnameChange} required error={this.state.firstnameError} />
          </Form.Field>
          <Form.Field>
            <label>Last Name</label>
            <Input placeholder="Last Name" name="lastName" minLength="1" maxLength="250" onChange={this.lastnameChange} required error={this.state.lastnameError} />
          </Form.Field>
          <Form.Field>
            <label>Email</label>
            <Input placeholder="Email" type="email" maxLength="250" name="email" onChange={this.emailChange} required error={this.state.emailError} />
          </Form.Field>
          <Form.Field>
            <label>Password</label>
            <Input placeholder="Password" type="password" maxLength="250" minLength="6" name="password" onChange={this.passwordChange} required error={this.state.passwordError} />
          </Form.Field>
          <Form.Field>
            <label>Password confirmation</label>
            <Input placeholder="Password confirmation" type="password" name="confirm" required maxLength="250" minLength="6"
              onChange={this.passConfirmChange} error={this.state.passwordConfirmError} />
          </Form.Field>
          <Form.Field>
            <Checkbox label='I agree to the Terms and Conditions' name="terms" checked={this.state.terms} onChange={this.termsChange} required />
          </Form.Field>
          <Button type='submit' onClick={this.submit}>Submit</Button>
        </Form>
      </Container>
    );
  }
}
