export default class User {
  private firstname: string
  private lastname: string
  private email: string
  private password: string

  getFirstname (): string {
    return this.firstname
  }

  getLastname (): string {
    return this.lastname
  }

  getEmail (): string {
    return this.email
  }

  setFirstname (value: string): User {
    this.firstname = value
    return this
  }

  setLastname (value: string): User {
    this.lastname = value
    return this
  }

  setEmail (value: string): User {
    this.email = value
    return this
  }

  setPassword (value: string): User {
    this.password = value
    return this
  }

  toJSON (): Object {
    return {
      firstname: this.getFirstname(),
      lastname: this.getLastname(),
      email: this.getEmail(),
      password: this.password
    }
  }
}