import React, { Component, ReactNode } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './app.css';
import RegisterUser from './page/user/RegisterUser';
import Home from './page/Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Nav from './component/Nav';


export default class App extends Component {
  render(): ReactNode {
    return (
      <Router>
        <main>
          <Nav />
        </main>
        <Switch >
          <Route path="/" exact component={Home} />
          <Route path="/register" component={RegisterUser} />
        </Switch>
      </Router>
    );
  }
}
