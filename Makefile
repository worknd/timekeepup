ifneq (,$(wildcard ./.env))
	include .env
	export
endif

UID=$(shell id -u)
GID=$(shell id -g)
USERNAME=$(shell whoami)

DOCKER_COMPOSE=$(shell which docker-compose) -f docker-compose.yml
DOCKER_NETWORK=docker network create focus-network --subnet 172.16.59.0/24

# Security
outdated:
	npm outdated

# Containers
setup:
	make network
	make build
	make start

build:
	$(DOCKER_COMPOSE) build --build-arg USERNAME=$(USERNAME) --build-arg USER_ID=$(UID) --force-rm

start:
	$(DOCKER_COMPOSE) up -d

stop:
	$(DOCKER_COMPOSE) down --remove-orphans
	make drop-network

network:
ifeq (,$(shell docker network ls | grep focus-network))
	$(DOCKER_NETWORK)
endif

drop-network:
	docker network rm focus-network

generate-ssl:
	openssl req -x509 -newkey rsa -keyout ./secrets/app_local.key -out ./secrets/app_local.crt -days 365 -config ssl.conf

clean-volumes:
	docker volume rm -f tk-db-data
