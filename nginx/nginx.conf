events { worker_connections 1024; }

http {
  upstream adminer_upstream {
    server adminer:8080;
    keepalive 64;
  }

  upstream app_upstream {
    server web:3000;
    keepalive 64;
  }

  server {
    listen 80;
    server_name adminer.local;

    location / {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $http_host;
      proxy_set_header X-NginX-Proxy true;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_max_temp_file_size 0;
      proxy_pass http://adminer_upstream/;
      proxy_redirect off;
      proxy_read_timeout 240s;
    }
  }

  server {
    listen 80;
    server_name app.local;
    return 301 https://$host$request_uri;
  }

  server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    server_name app.local;

    ssl_certificate /etc/ssl/certs/app_local.crt;
    ssl_certificate_key /etc/ssl/private/app_local.key;

    ssl_protocols TLSv1.2 TLSv1.1 TLSv1;

    location / {
      proxy_set_header   X-Forwarded-For $remote_addr;
      proxy_set_header   Host $http_host;
      proxy_set_header X-NginX-Proxy true;
      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
      proxy_max_temp_file_size 0;
      proxy_pass https://app_upstream/;
      proxy_redirect off;
      proxy_read_timeout 240s;
      proxy_ssl_certificate /etc/ssl/certs/app_local.crt;
      proxy_ssl_certificate_key /etc/ssl/private/app_local.key;
    }
  }
}
