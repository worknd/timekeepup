module.exports = function (api) {
  api.cache(true);

  const presets = [
    ["@babel/preset-env", {
      useBuiltIns: "entry",
      corejs: 3
    }],
    "@babel/preset-react",
    "@babel/preset-typescript"
  ];

  const plugins = [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-transform-modules-commonjs'
  ];

  return {
    presets,
    plugins
  };
}
